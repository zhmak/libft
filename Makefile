# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/15 17:48:22 by eloren-l          #+#    #+#              #
#    Updated: 2018/12/28 19:41:42 by eloren-l         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# BASIC DIRECTORIES
SRC_DIR = srcs
INC_DIR = includes
OBJ_DIR = objs

# PATHS TO SRC SUBDIRECTORIES & NAMES OF SUBDIRECTORIES
SRC_SUBDIR = $(wildcard $(SRC_DIR)/*)
SUBDIR = $(SRC_SUBDIR:$(SRC_DIR)/%=%)

# PATHS TO C SOURCE FILES
SRC_PATH = $(foreach subdir,$(SRC_SUBDIR),$(wildcard $(subdir)/*.c))

# PATHS TO OBJ SUBDIRECTRIES, PATHS TO & NAMES OF OBJECTS
OBJ_SUBDIR = $(addprefix $(OBJ_DIR)/,$(SUBDIR))
OBJ_PATH = $(SRC_PATH:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# NAME OF LIBRARY TO COMPILE
NAME = libft.a

# COMPILATION RULES
CC = gcc
FLAGS = -Wall -Wextra -Werror


.PHONY: all clean fclean re

all: $(OBJ_SUBDIR) $(NAME)

$(NAME): $(OBJ_PATH)
	ar rc $@ $^
	ranlib $@

$(OBJ_SUBDIR):
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(FLAGS) -o $@ -c $< -I$(INC_DIR)

clean:
	rm -rf $(OBJ_DIR)

fclean: clean
	rm -f $(NAME)

re: fclean all